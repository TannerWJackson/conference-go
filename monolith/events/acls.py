from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers, params=params)
    photo = json.loads(response.content)

    try:
        photo_info = {
            "picture_url": photo["photos"][0]["src"]["original"],
            "photographer": photo["photos"][0]["photographer"],
            # I just think it's polite to credit photographers for their work.
        }
    except KeyError or IndexError:
        photo_info = {
            "picture_url": None,
            "photographer": None,
        }

    return photo_info


def get_weather(city, state):
    geolocate_url = (
        "http://api.openweathermap.org/geo/1.0/direct?q="
        + city
        + ","
        + state
        + ",United States of America&limit=1&appid="
        + OPEN_WEATHER_API_KEY
    )
    geolocate_response = requests.get(geolocate_url)
    geolocate_data = json.loads(geolocate_response.content)

    try:
        lat_and_lon = {
            "latitude": geolocate_data[0]["lat"],
            "longitude": geolocate_data[0]["lon"],
        }

        weather_url = (
            "https://api.openweathermap.org/data/2.5/weather?lat="
            + str(lat_and_lon["latitude"])
            + "&lon="
            + str(lat_and_lon["longitude"])
            + "&appid="
            + OPEN_WEATHER_API_KEY
        )

        weather_response = requests.get(weather_url)
        weather_data = json.loads(weather_response.content)

        weather_info = {
            "temperature": (weather_data["main"]["temp"] - 273.15) * (9 / 5)
            + 32,
            "description": weather_data["weather"][0]["description"],
        }
    except IndexError or KeyError:
        weather_info = None

    return weather_info
